Personal portfolio website.

The [GitLab Pages](https://vincentmacri.gitlab.io) hosting is just used for testing purposes. The current working version of the site is at [vincemacri.ca](https://vincemacri.ca).
